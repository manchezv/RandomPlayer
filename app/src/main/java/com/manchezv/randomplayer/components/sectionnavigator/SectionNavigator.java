package com.manchezv.randomplayer.components.sectionnavigator;

public interface SectionNavigator {

    void navigateToPlayer();

    void navigateToProgress();

    void navigateToSummary();
}
