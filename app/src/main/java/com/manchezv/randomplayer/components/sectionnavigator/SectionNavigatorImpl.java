package com.manchezv.randomplayer.components.sectionnavigator;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.manchezv.randomplayer.R;
import com.manchezv.randomplayer.views.progress.ProgressFragment;
import com.manchezv.randomplayer.views.summary.SummaryFragment;
import com.manchezv.randomplayer.views.player.PlayerFragment;

import javax.inject.Inject;

public class SectionNavigatorImpl implements SectionNavigator {

    private static final String PLAYER_TAG = "PLAYERTAG";
    private static final String PROGRESS_TAG = "PROGRESSTAG";
    private static final String SUMMARY_TAG = "SUMMARYTAG";

    private final AppCompatActivity activity;

    @Inject
    public SectionNavigatorImpl(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void navigateToPlayer() {
        if(!isCurrentFragment(PLAYER_TAG)) {
            replaceFragment(PlayerFragment.Companion.newInstance(), PLAYER_TAG);
        }
    }

    @Override
    public void navigateToProgress() {
        if(!isCurrentFragment(PROGRESS_TAG)) {
            replaceFragment(ProgressFragment.Companion.newInstance(), PROGRESS_TAG);
        }
    }

    @Override
    public void navigateToSummary() {
        if(!isCurrentFragment(SUMMARY_TAG)) {
            replaceFragment(SummaryFragment.Companion.newInstance(), SUMMARY_TAG);
        }
    }

    private boolean isCurrentFragment(String progressTag) {
        Fragment fragment = activity.getSupportFragmentManager().findFragmentByTag(progressTag);
        return (fragment != null && fragment.isVisible());
    }

    private void replaceFragment(Fragment fragment, String tag) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(
                        R.id.lyContent,
                        fragment,
                        tag
                )
                .commit();
    }
}
