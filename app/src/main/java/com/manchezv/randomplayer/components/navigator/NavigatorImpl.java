package com.manchezv.randomplayer.components.navigator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.manchezv.randomplayer.views.settings.SettingsActivity;

import javax.inject.Inject;

public class NavigatorImpl implements Navigator {

    private final AppCompatActivity activity;

    @Inject
    public NavigatorImpl(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void goToSettings() {
        Intent i = new Intent(activity, SettingsActivity.class);
        activity.startActivity(i);
    }
}
