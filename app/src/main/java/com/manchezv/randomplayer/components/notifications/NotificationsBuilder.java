package com.manchezv.randomplayer.components.notifications;

import android.app.Notification;

public interface NotificationsBuilder {
    Notification buildOngoingNotification();
}
