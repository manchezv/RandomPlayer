package com.manchezv.randomplayer.viewmodels.summary;

import com.manchezv.randomplayer.local.db.model.GeneratedNumberEntity;
import com.manchezv.randomplayer.util.EntityMapper;

import javax.inject.Inject;

public class SummaryRowMapper extends EntityMapper<GeneratedNumberEntity, SummaryRowModel> {

    private final SummaryDateFormat dateFormat;

    @Inject
    public SummaryRowMapper(SummaryDateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }

    @Override
    protected GeneratedNumberEntity mapOperation(SummaryRowModel element) {
        throw new RuntimeException("SummaryRowMapper map operation not implemented");
    }

    @Override
    protected SummaryRowModel reverseMapOperation(GeneratedNumberEntity element) {
        return new SummaryRowModel(element.getId(), element.getValue(), dateFormat.format(element.getGeneratedTime()));
    }
}
