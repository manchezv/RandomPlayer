package com.manchezv.randomplayer.viewmodels.summary;

import java.text.SimpleDateFormat;

import javax.inject.Inject;

public class SummaryDateFormat extends SimpleDateFormat {

    private static final String summaryFormat = "yyyy-MM-dd HH:mm:ss.SSS";

    @Inject
    public SummaryDateFormat() {
        super(summaryFormat);
    }
}
