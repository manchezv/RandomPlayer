package com.manchezv.randomplayer.viewmodels.summary;

import java.util.Date;

public class SummaryRowModel {
    private long id;
    private int value;
    private String generatedTime;

    public SummaryRowModel(long id, int value, String generatedTime) {
        this.id = id;
        this.value = value;
        this.generatedTime = generatedTime;
    }

    public long getId() {
        return id;
    }

    public int getValue() {
        return value;
    }

    public String getGeneratedTime() {
        return generatedTime;
    }
}
