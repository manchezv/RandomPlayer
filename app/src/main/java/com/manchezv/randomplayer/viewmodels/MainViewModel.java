package com.manchezv.randomplayer.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.manchezv.randomplayer.cases.UseCaseResponse;
import com.manchezv.randomplayer.cases.numbers.calculation.CalculationData;
import com.manchezv.randomplayer.cases.numbers.calculation.GetCalculationData;
import com.manchezv.randomplayer.cases.numbers.delete.DeleteNumbersUseCase;
import com.manchezv.randomplayer.cases.numbers.insert.InsertNumberUseCase;
import com.manchezv.randomplayer.local.db.model.GeneratedNumberEntity;
import com.manchezv.randomplayer.repositories.GeneratedNumbersRepository;
import com.manchezv.randomplayer.viewmodels.summary.SummaryRowMapper;
import com.manchezv.randomplayer.viewmodels.summary.SummaryRowModel;

import java.util.List;

import javax.inject.Inject;

public class MainViewModel extends ViewModel {

    private final GeneratedNumbersRepository numRepo;

    private final GetCalculationData calculateUseCase;

    private final InsertNumberUseCase insertUseCase;

    private final LiveData<List<GeneratedNumberEntity>> numberListData;

    private final LiveData<List<SummaryRowModel>> summaryRowModel;

    private final LiveData<UseCaseResponse<CalculationData>> calculationData;

    private final DeleteNumbersUseCase deleteNumbersUseCase;

    @Inject
    public MainViewModel(GeneratedNumbersRepository numRepo, GetCalculationData calculateUseCase, InsertNumberUseCase insertUseCase,
                         DeleteNumbersUseCase deleteNumbersUseCase,
                         SummaryRowMapper rowMapper) {
        this.numRepo = numRepo;
        this.calculateUseCase = calculateUseCase;
        this.insertUseCase = insertUseCase;
        this.deleteNumbersUseCase = deleteNumbersUseCase;
        numberListData = numRepo.getNumberList();
        calculationData = Transformations.switchMap(numberListData, generatedList -> {
            MutableLiveData<UseCaseResponse<CalculationData>> liveData = new MutableLiveData<>();
            this.calculateUseCase.execute(generatedList, liveData::setValue);
            return liveData;
        });

        summaryRowModel = Transformations.map(numberListData, rowMapper::reverseMapList);
    }

    public LiveData<UseCaseResponse<CalculationData>> getCalculationData() {
        return calculationData;
    }

    public LiveData<List<GeneratedNumberEntity>> getNumberListData() {
        return numberListData;
    }

    public void onStopClicked() {
        deleteNumbersUseCase.execute(null, response -> {

        });
    }

    public LiveData<List<SummaryRowModel>> getSummaryRowModel() {
        return summaryRowModel;
    }
}
