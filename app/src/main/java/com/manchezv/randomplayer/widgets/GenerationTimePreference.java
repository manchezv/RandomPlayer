package com.manchezv.randomplayer.widgets;

import android.content.Context;
import android.os.Build;
import android.preference.EditTextPreference;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;

import com.manchezv.randomplayer.R;

public class GenerationTimePreference extends EditTextPreference {

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public GenerationTimePreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public GenerationTimePreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public GenerationTimePreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GenerationTimePreference(Context context) {
        super(context);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult && getEditText().getText().toString().isEmpty()) {
            getEditText().setText(String.valueOf(getContext().getResources().getInteger(R.integer.default_interval)));
        }
        super.onDialogClosed(positiveResult);

    }
}
