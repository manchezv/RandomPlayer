package com.manchezv.randomplayer.views.service;

public enum ServiceState {
    READY, RUNNING, PAUSED, STOPPED, DISCONNECTED
}
