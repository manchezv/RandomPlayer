package com.manchezv.randomplayer.views.service;

import android.arch.lifecycle.LiveData;

import org.jetbrains.annotations.Nullable;

public interface INumGeneratorService {
    void onStartClicked();

    void onPauseClicked();

    void onStopClicked();

    boolean isGeneratingNumbers();

    @Nullable
    LiveData<ServiceState> getState();
}
