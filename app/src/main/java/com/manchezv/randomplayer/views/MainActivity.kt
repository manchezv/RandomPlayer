package com.manchezv.randomplayer.views

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModelProviders
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.support.design.widget.BottomNavigationView
import android.view.Menu
import android.view.MenuItem
import com.manchezv.randomplayer.R
import com.manchezv.randomplayer.components.navigator.Navigator
import com.manchezv.randomplayer.components.sectionnavigator.SectionNavigator
import com.manchezv.randomplayer.viewmodels.MainViewModel
import com.manchezv.randomplayer.views.service.INumGeneratorService
import com.manchezv.randomplayer.views.service.NumGeneratorService
import com.manchezv.randomplayer.views.service.ServiceState
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_player.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainView {

    override fun onStopClicked() {
        iService?.onStopClicked()
    }

    override fun stopGeneratingNumbers() {
        iService?.onPauseClicked()
    }

    override fun startGeneratingNumbers() {
        iService?.onStartClicked()
    }

    override fun bindService(): LiveData<ServiceState> {
        return serviceState
    }

    private lateinit var viewModel: MainViewModel

    private var iService: INumGeneratorService? = null

    private val serviceState: MutableLiveData<ServiceState> = MutableLiveData()

    private val serviceConnection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            iService = (service as NumGeneratorService.NumGeneratorServiceBinder).numService
            iService!!.state?.observe(this@MainActivity, Observer {
                serviceState.value = it
            })
        }

        override fun onServiceDisconnected(name: ComponentName) {
            serviceState.value = ServiceState.DISCONNECTED
            iService = null
        }
    }

    @Inject
    lateinit var sectionNavigator: SectionNavigator

    @Inject
    lateinit var navigator: Navigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(toolbar)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

        if (savedInstanceState == null) {
            navigation.selectedItemId = R.id.nav_player
        }

        if (iService == null) {
            startService(Intent(this, NumGeneratorService::class.java));
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_settings -> {
                navigator.goToSettings();
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onStart() {
        super.onStart()
        bindService(Intent(this, NumGeneratorService::class.java), serviceConnection, 0)
    }

    override fun onStop() {
        super.onStop()
        unbindService(serviceConnection)
    }

    override fun initDagger() {
        AndroidInjection.inject(this);
    }

    override fun getLayoutResId(): Int {
        return R.layout.activity_main
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.nav_player -> {
                sectionNavigator.navigateToPlayer();
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_progress -> {
                sectionNavigator.navigateToProgress();
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_summary -> {
                sectionNavigator.navigateToSummary();
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }
}
