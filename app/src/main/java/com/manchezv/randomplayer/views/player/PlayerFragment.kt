package com.manchezv.randomplayer.views.player


import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.manchezv.randomplayer.R
import com.manchezv.randomplayer.viewmodels.MainViewModel
import com.manchezv.randomplayer.views.BaseFragment
import com.manchezv.randomplayer.views.MainView
import com.manchezv.randomplayer.views.service.ServiceState
import kotlinx.android.synthetic.main.fragment_player.*


/**
 * A simple [Fragment] subclass.
 *
 */
class PlayerFragment : BaseFragment() {

    companion object {
        fun newInstance(): PlayerFragment {
            return PlayerFragment()
        }
    }

    private var mainView: MainView? = null

    private var serviceState: LiveData<ServiceState>? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is MainView) {
            this.mainView = context;
        } else {
            throw RuntimeException("Activity must implement MainView Interface")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mainView = null
    }

    private val viewModel: MainViewModel by lazy {
        ViewModelProviders.of(requireActivity()).get(MainViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        textView_value.text = getString(R.string.null_value)
        textview_count.text = getString(R.string.null_value)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.numberListData.observe(this, Observer {
            textview_count.text = it?.size.toString()
            textView_value.text = if (it?.firstOrNull() != null) it[0].value.toString() else getText(R.string.null_value)
            stop_button.isEnabled = it != null && !it.isEmpty()
        })

        serviceState = mainView?.bindService()

        serviceState?.observe(this, Observer {
            when (it) {
                ServiceState.READY -> {
                    play_button.isEnabled = true
                    pause_button.isEnabled = false
                }
                ServiceState.RUNNING -> {
                    pause_button.isEnabled = true
                    play_button.isEnabled = false
                }
                ServiceState.PAUSED -> {
                    play_button.isEnabled = true
                    pause_button.isEnabled = false
                }
                ServiceState.STOPPED -> {
                    pause_button.isEnabled = false
                    play_button.isEnabled = true
                }
                else -> {
                    play_button.isEnabled = false
                    pause_button.isEnabled = false
                }
            }
        })

        play_button.setOnClickListener {
            mainView?.startGeneratingNumbers()
        }

        pause_button.setOnClickListener {
            mainView?.stopGeneratingNumbers()
        }

        stop_button.setOnClickListener {
            mainView?.onStopClicked()
            viewModel.onStopClicked()
        }

    }

    override fun getResId(): Int {
        return R.layout.fragment_player
    }


}
