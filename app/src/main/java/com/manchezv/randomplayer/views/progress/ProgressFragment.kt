package com.manchezv.randomplayer.views.progress


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.manchezv.randomplayer.R
import com.manchezv.randomplayer.viewmodels.MainViewModel
import com.manchezv.randomplayer.views.BaseFragment
import kotlinx.android.synthetic.main.fragment_progress.*
import android.animation.ObjectAnimator
import android.view.animation.DecelerateInterpolator


/**
 * A simple [Fragment] subclass.
 *
 */
class ProgressFragment : BaseFragment() {

    companion object {
        fun newInstance(): ProgressFragment {
            return ProgressFragment()
        }
    }

    private val viewModel: MainViewModel by lazy {
        ViewModelProviders.of(requireActivity()).get(MainViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        textview_currentvalue.text = getString(R.string.null_value)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        viewModel.numberListData.observe(this, Observer {
            textview_currentvalue.text = if (it?.firstOrNull() != null) it[0].value.toString() else getText(R.string.null_value)

            if (it?.isEmpty() == false) {
                val progressAnimator = ObjectAnimator.ofInt(progressBar, "progress", progressBar.progress, it[0].value)
                progressAnimator.setDuration(resources.getInteger(android.R.integer.config_shortAnimTime).toLong())
                progressAnimator.setInterpolator(DecelerateInterpolator())
                progressAnimator.start()
            }

        })

    }

    override fun getResId(): Int {
        return R.layout.fragment_progress
    }


}
