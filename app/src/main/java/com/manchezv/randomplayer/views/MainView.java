package com.manchezv.randomplayer.views;

import android.arch.lifecycle.LiveData;

import com.manchezv.randomplayer.views.service.ServiceState;

import org.jetbrains.annotations.NotNull;

public interface MainView {
    void onStopClicked();

    void stopGeneratingNumbers();

    void startGeneratingNumbers();

    LiveData<ServiceState> bindService();
}
