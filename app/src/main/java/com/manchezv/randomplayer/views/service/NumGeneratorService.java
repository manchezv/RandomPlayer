package com.manchezv.randomplayer.views.service;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.manchezv.randomplayer.cases.numbers.insert.InsertNumberUseCase;
import com.manchezv.randomplayer.components.notifications.NotificationsBuilder;
import com.manchezv.randomplayer.local.db.model.GeneratedNumberEntity;

import org.jetbrains.annotations.Nullable;

import java.util.Calendar;
import java.util.Random;

import javax.inject.Inject;

import dagger.android.DaggerService;

public class NumGeneratorService extends DaggerService implements INumGeneratorService {

    private static final int ONGOING_NOTIFICATION_ID = 2999;

    @Inject
    InsertNumberUseCase insertNumberUseCase;
    
    @Inject
    NotificationsBuilder notificationBuilder;

    private final MutableLiveData<ServiceState> state = new MutableLiveData<>();

    private IBinder binder = new NumGeneratorServiceBinder();

    private boolean isGeneratingNumbers = false;

    public class NumGeneratorServiceBinder extends Binder {
        public INumGeneratorService getNumService() {
            return NumGeneratorService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        state.setValue(ServiceState.READY);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onStartClicked() {
        isGeneratingNumbers = true;
        state.setValue(ServiceState.RUNNING);
        startInForegroundMode();
        Random rand = new Random();
        insertNumber(rand);
    }

    private void startInForegroundMode() {
        startForeground(ONGOING_NOTIFICATION_ID, notificationBuilder.buildOngoingNotification());
    }

    @Override
    public void onPauseClicked() {
        isGeneratingNumbers = false;
        state.setValue(ServiceState.PAUSED);
        stopForeground(true);
    }

    @Override
    public void onStopClicked() {
        isGeneratingNumbers = false;
        state.setValue(ServiceState.STOPPED);
        stopForeground(true);
    }

    @Override
    public boolean isGeneratingNumbers() {
        return isGeneratingNumbers;
    }

    @Nullable
    @Override
    public LiveData<ServiceState> getState() {
        return state;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    private void insertNumber(Random rand) {
        if (isGeneratingNumbers) {
            insertNumberUseCase.execute(new GeneratedNumberEntity(rand.nextInt(100 - 20) + 20, Calendar.getInstance().getTime()), response -> insertNumber(rand));
        }
    }
}
