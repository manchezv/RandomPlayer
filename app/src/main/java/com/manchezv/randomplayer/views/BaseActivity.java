package com.manchezv.randomplayer.views;

import android.arch.lifecycle.ViewModelProvider;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

public abstract class BaseActivity extends AppCompatActivity {

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutResId());
        initDagger();
    }

    public ViewModelProvider.Factory getViewModelFactory() {
        return viewModelFactory;
    }

    protected abstract void initDagger();

    public abstract int getLayoutResId();
}
