package com.manchezv.randomplayer.views.summary


import android.arch.lifecycle.Observer
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.manchezv.randomplayer.R
import com.manchezv.randomplayer.notifications.NotCalculableNotification
import com.manchezv.randomplayer.viewmodels.MainViewModel
import com.manchezv.randomplayer.views.BaseFragment
import kotlinx.android.synthetic.main.fragment_summary.*
import java.util.*


/**
 * A simple [Fragment] subclass.
 *
 */
class SummaryFragment : BaseFragment() {

    companion object {
        fun newInstance(): SummaryFragment {
            return SummaryFragment()
        }
    }

    private lateinit var adapter: SummaryAdapter

    private val viewModel: MainViewModel by lazy {
        ViewModelProviders.of(requireActivity()).get(MainViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val noValue = getString(R.string.null_value)

        textView_value.text = noValue
        textview_median.text = getString(R.string.null_value)
        textview_desviation.text = getString(R.string.null_value)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = SummaryAdapter(context)

        rvHistory.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = this@SummaryFragment.adapter
        }

        viewModel.summaryRowModel.observe(this, Observer {
            adapter.submitList(it);
        })

        adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                rvHistory?.scrollToPosition(positionStart + itemCount - 1);
            }
        })


        viewModel.numberListData.observe(this, Observer {
            textView_value.text = if (it?.firstOrNull() != null) it[0].value.toString() else getText(R.string.null_value)
        })

        viewModel.calculationData.observe(this, Observer {
            if (it != null) {
                if (it.hasNotification() && it.notification is NotCalculableNotification) {
                    textview_median.text = getString(R.string.null_value)
                    textview_desviation.text = getString(R.string.null_value)

                } else {
                    textview_median.text = String.format(Locale.getDefault(), it.result.median.toString())
                    textview_desviation.text = String.format(Locale.getDefault(), it.result.desviation.toString())
                }
            } else {
                textview_median.text = getString(R.string.null_value)
                textview_desviation.text = getString(R.string.null_value)
            }
        })

    }

    override fun getResId(): Int {
        return R.layout.fragment_summary
    }


}
