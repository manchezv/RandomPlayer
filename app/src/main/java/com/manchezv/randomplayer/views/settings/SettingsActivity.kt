package com.manchezv.randomplayer.views.settings

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.manchezv.randomplayer.R
import com.manchezv.randomplayer.views.BaseActivity
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.settings)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun initDagger() {

    }

    override fun getLayoutResId(): Int {
        return R.layout.activity_settings
    }
}
