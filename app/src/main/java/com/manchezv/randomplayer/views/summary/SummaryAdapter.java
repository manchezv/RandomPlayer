package com.manchezv.randomplayer.views.summary;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.manchezv.randomplayer.R;
import com.manchezv.randomplayer.viewmodels.summary.SummaryRowModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SummaryAdapter extends ListAdapter<SummaryRowModel, SummaryAdapter.SummaryViewHolder> {

    private LayoutInflater layoutInflater;

    public SummaryAdapter(Context context) {
        super(DIFF_CALLBACK);
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public SummaryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.history_item_layout, parent, false);
        return new SummaryViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull SummaryViewHolder holder, int position) {
            holder.bindItem(getItem(position));
    }

    public static final DiffUtil.ItemCallback<SummaryRowModel> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<SummaryRowModel>() {
                @Override
                public boolean areItemsTheSame(SummaryRowModel oldItem, SummaryRowModel newItem) {
                    return oldItem.getId() == newItem.getId();
                }

                @Override
                public boolean areContentsTheSame(SummaryRowModel oldItem, SummaryRowModel newItem) {
                    return oldItem.getId() == newItem.getId() && oldItem.getValue() == newItem.getValue() && oldItem.getGeneratedTime().contentEquals(newItem.getGeneratedTime());
                }
            };


    class SummaryViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tvGeneratedTime)
        TextView tvGeneratedTime;

        @BindView(R.id.tvValue)
        TextView tvValue;

        SummaryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindItem(SummaryRowModel numberEntity) {
            tvGeneratedTime.setText(numberEntity.getGeneratedTime());
            tvValue.setText(String.valueOf(numberEntity.getValue()));
        }
    }
}
