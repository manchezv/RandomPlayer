package com.manchezv.randomplayer;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.preference.PreferenceManager;

import com.manchezv.randomplayer.injection.ApplicationModule;
import com.manchezv.randomplayer.injection.DaggerApplicationComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasServiceInjector;

public class RandomPlayerApplication extends Application implements HasActivityInjector, HasServiceInjector {

    @Inject
    DispatchingAndroidInjector<Activity> activityInjector;

    @Inject
    DispatchingAndroidInjector<Service> serviceInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        prepareInjection();

        PreferenceManager.setDefaultValues(this, R.xml.settings_preference, false);
    }

    private void prepareInjection() {
        DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build()
                .inject(this);

    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityInjector;
    }

    @Override
    public AndroidInjector<Service> serviceInjector() {
        return serviceInjector;
    }
}
