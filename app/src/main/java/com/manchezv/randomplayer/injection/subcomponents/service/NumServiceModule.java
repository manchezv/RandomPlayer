package com.manchezv.randomplayer.injection.subcomponents.service;

import android.content.Context;

import com.manchezv.randomplayer.components.notifications.NotificationsBuilder;
import com.manchezv.randomplayer.components.notifications.NotificationsBuilderImpl;
import com.manchezv.randomplayer.local.preferences.PreferencesGateway;
import com.manchezv.randomplayer.local.preferences.PreferencesGatewayImpl;
import com.manchezv.randomplayer.views.service.NumGeneratorService;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class NumServiceModule {

    @Binds
    abstract Context providesContext(NumGeneratorService service);

    @Binds
    abstract NotificationsBuilder providesNotificationsBuilder(NotificationsBuilderImpl not);

}
