package com.manchezv.randomplayer.injection;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.manchezv.randomplayer.RandomPlayerApplication;
import com.manchezv.randomplayer.local.db.GeneratedNumberDAO;
import com.manchezv.randomplayer.local.db.RandomPlayerDatabase;
import com.manchezv.randomplayer.local.preferences.PreferencesGateway;
import com.manchezv.randomplayer.local.preferences.PreferencesGatewayImpl;
import com.manchezv.randomplayer.threading.MainThread;
import com.manchezv.randomplayer.threading.MainThreadImpl;
import com.manchezv.randomplayer.threading.UseCaseExecutor;
import com.manchezv.randomplayer.threading.UseCaseExecutorImpl;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private Application application;

    public ApplicationModule(RandomPlayerApplication app) {
        application = app;
    }

    @Provides
    @Singleton
    public Application providesApplication() {
        return application;
    }

    @Provides
    @Singleton
    public MainThread providesMainThread(MainThreadImpl mainThread) {
        return mainThread;
    }

    @Provides
    @Singleton
    public UseCaseExecutor providesUseCaseExecutor(UseCaseExecutorImpl useCaseExecutor) {
        return useCaseExecutor;
    }


    @Provides
    @Singleton
    RandomPlayerDatabase providesRandomPlayerDatabase(Application app) {
        return Room.databaseBuilder(app, RandomPlayerDatabase.class, "randomplayer_db")
                .build();
    }

    @Provides
    @Singleton
    GeneratedNumberDAO providesGeneratedNumberDao(RandomPlayerDatabase db) {
        return db.getGeneratedNumberDao();
    }


    @Provides
    @Singleton
    public SharedPreferences providesSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Provides
    PreferencesGateway providesPreferencesGateway(PreferencesGatewayImpl gateway) {
        return gateway;
    }

}
