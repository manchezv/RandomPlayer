package com.manchezv.randomplayer.injection.builder;

import com.manchezv.randomplayer.injection.subcomponents.main.MainActivityModule;
import com.manchezv.randomplayer.views.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivitiesBuilderModule {

    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity injectsMainActivity();

}
