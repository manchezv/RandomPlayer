package com.manchezv.randomplayer.injection.subcomponents.main;

import android.support.v7.app.AppCompatActivity;

import com.manchezv.randomplayer.components.navigator.Navigator;
import com.manchezv.randomplayer.components.navigator.NavigatorImpl;
import com.manchezv.randomplayer.components.sectionnavigator.SectionNavigator;
import com.manchezv.randomplayer.components.sectionnavigator.SectionNavigatorImpl;
import com.manchezv.randomplayer.repositories.GeneratedNumbersRepository;
import com.manchezv.randomplayer.repositories.GeneratedNumbersRepositoryImpl;
import com.manchezv.randomplayer.views.MainActivity;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class MainActivityModule {

    @Binds
    abstract AppCompatActivity providesAppCompatActivity(MainActivity activity);

    @Binds
    abstract SectionNavigator providesSectionNavigator(SectionNavigatorImpl sectionNavigator);

    @Binds
    abstract Navigator providesNavigator(NavigatorImpl navigator);

}
