package com.manchezv.randomplayer.injection.usecases;

import com.manchezv.randomplayer.cases.numbers.calculation.GetCalculationData;
import com.manchezv.randomplayer.cases.numbers.calculation.GetCalculationDataUseCaseImpl;
import com.manchezv.randomplayer.cases.numbers.delete.DeleteNumbersUseCase;
import com.manchezv.randomplayer.cases.numbers.delete.DeleteNumbersUseCaseImpl;
import com.manchezv.randomplayer.cases.numbers.insert.InsertNumberUseCase;
import com.manchezv.randomplayer.cases.numbers.insert.InsertNumberUseCaseImpl;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class UseCasesModule {

    @Binds
    abstract GetCalculationData providesGetMedianUseCase(GetCalculationDataUseCaseImpl useCase);

    @Binds
    abstract InsertNumberUseCase providesInsertNumberUseCase(InsertNumberUseCaseImpl useCase);

    @Binds
    abstract DeleteNumbersUseCase providesDeleteNumbersUseCase(DeleteNumbersUseCaseImpl useCase);

}
