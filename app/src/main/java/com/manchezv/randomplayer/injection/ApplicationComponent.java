package com.manchezv.randomplayer.injection;

import com.manchezv.randomplayer.RandomPlayerApplication;
import com.manchezv.randomplayer.injection.builder.ActivitiesBuilderModule;
import com.manchezv.randomplayer.injection.builder.ServicesBuilderModule;
import com.manchezv.randomplayer.injection.repository.RepositoryModule;
import com.manchezv.randomplayer.injection.usecases.UseCasesModule;
import com.manchezv.randomplayer.injection.viewmodels.ViewModelsModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Component(
        modules = {
                ApplicationModule.class,
                AndroidInjectionModule.class,
                ActivitiesBuilderModule.class,
                ServicesBuilderModule.class,
                RepositoryModule.class,
                UseCasesModule.class,
                ViewModelsModule.class
        }
)
@Singleton
public interface ApplicationComponent {

    void inject(RandomPlayerApplication app);


}
