package com.manchezv.randomplayer.injection.repository;

import com.manchezv.randomplayer.repositories.GeneratedNumbersRepository;
import com.manchezv.randomplayer.repositories.GeneratedNumbersRepositoryImpl;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class RepositoryModule {

    @Binds
    abstract GeneratedNumbersRepository providesGeneratedNumbersRepository(GeneratedNumbersRepositoryImpl repo);
}
