package com.manchezv.randomplayer.injection.builder;

import com.manchezv.randomplayer.injection.subcomponents.service.NumServiceModule;
import com.manchezv.randomplayer.views.service.NumGeneratorService;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ServicesBuilderModule {

    @ContributesAndroidInjector(modules = NumServiceModule.class)
    abstract NumGeneratorService injectsNumGeneratorService();
}
