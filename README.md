# RandomPlayer
## Introduction
RandomPlayer is a sample app which is conformed by 3 different screens joined by a bottom navigation:

- First screen control the number generation background service, starting, pausing or stopping the dataset and the service.
- Second screen shows the progression between 2 numbers generated with a progressbar and some simple animation.
- Last screen shows some arithmetical calculations like median and desviation with current generated value and a list with the dataset, scrolling to the last generated number.

## Technologies
This project has a sample test of the new Android Architecture Components and observable classes to update 3 screens from dataset.

The project uses next components and languages:
- Kotlin
- Java
- ViewModel
- MVVM pattern
- Room
- LiveData (observable)
- Bottom Navigation