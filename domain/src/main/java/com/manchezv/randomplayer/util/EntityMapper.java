package com.manchezv.randomplayer.util;

import java.util.ArrayList;
import java.util.List;

public abstract class EntityMapper<T, K> {

    /**
     * Map an element.
     *
     * @param element Element.
     * @return mapped class.
     */
    public final T map(K element) {
        if (element == null) {
            return null;
        }

        return mapOperation(element);
    }

    protected abstract T mapOperation(K element);

    /**
     * Reverse map for an element.
     *
     * @param element Object.
     * @return mapper class.
     */
    public final K reverseMap(T element) {
        if (element == null) {
            return null;
        }

        return reverseMapOperation(element);
    }

    protected abstract K reverseMapOperation(T element);

    /**
     * Map a list of elements.
     *
     * @param list List.
     * @return Mapped list.
     */
    public final List<T> mapList(List<K> list) {

        if (list != null) {

            List<T> returnedList = new ArrayList<>();

            for (K element : list) {

                returnedList.add(map(element));

            }

            return returnedList;

        }

        return null;
    }

    /**
     * Reverse mapping for a list of elements.
     *
     * @param list List.
     * @return Mapped list.
     */
    public final List<K> reverseMapList(List<T> list) {

        if (list != null) {
            List<K> returnedList = new ArrayList<>();

            for (T element : list) {

                returnedList.add(reverseMap(element));

            }

            return returnedList;
        }

        return null;

    }

}
