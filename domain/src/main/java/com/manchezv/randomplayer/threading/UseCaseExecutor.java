package com.manchezv.randomplayer.threading;

public interface UseCaseExecutor {
    void execute(Runnable runnable);
}
