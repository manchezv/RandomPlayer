package com.manchezv.randomplayer.threading;

public interface MainThread {
    void runOnUiThread(Runnable runnable);
}
