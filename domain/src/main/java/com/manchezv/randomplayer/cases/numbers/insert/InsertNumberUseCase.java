package com.manchezv.randomplayer.cases.numbers.insert;

import com.manchezv.randomplayer.cases.UseCase;
import com.manchezv.randomplayer.cases.UseCaseResponse;
import com.manchezv.randomplayer.local.db.model.GeneratedNumberEntity;

public interface InsertNumberUseCase extends UseCase<GeneratedNumberEntity, UseCaseResponse<Boolean>> {
}
