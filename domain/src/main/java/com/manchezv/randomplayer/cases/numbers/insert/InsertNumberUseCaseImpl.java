package com.manchezv.randomplayer.cases.numbers.insert;

import com.manchezv.randomplayer.cases.BaseUseCase;
import com.manchezv.randomplayer.cases.UseCaseResponse;
import com.manchezv.randomplayer.local.db.model.GeneratedNumberEntity;
import com.manchezv.randomplayer.local.preferences.PreferencesGateway;
import com.manchezv.randomplayer.repositories.GeneratedNumbersRepository;
import com.manchezv.randomplayer.threading.MainThread;
import com.manchezv.randomplayer.threading.UseCaseExecutor;

import javax.inject.Inject;

public class InsertNumberUseCaseImpl extends BaseUseCase<GeneratedNumberEntity, UseCaseResponse<Boolean>> implements InsertNumberUseCase {

    private final GeneratedNumbersRepository repo;
    private final PreferencesGateway preferencesGateway;

    @Inject
    public InsertNumberUseCaseImpl(UseCaseExecutor executor, MainThread mainThread, GeneratedNumbersRepository repo, PreferencesGateway preferencesGateway) {
        super(executor, mainThread);
        this.repo = repo;
        this.preferencesGateway = preferencesGateway;
    }

    @Override
    protected void run(GeneratedNumberEntity generatedNumber) {
        try {
            repo.insert(generatedNumber);
            Thread.sleep(preferencesGateway.getIntervalGenerationTime());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onResponse(new UseCaseResponse<>(true));
    }
}
