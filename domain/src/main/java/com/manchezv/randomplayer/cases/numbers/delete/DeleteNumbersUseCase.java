package com.manchezv.randomplayer.cases.numbers.delete;

import com.manchezv.randomplayer.cases.UseCase;
import com.manchezv.randomplayer.cases.UseCaseResponse;

public interface DeleteNumbersUseCase extends UseCase<Void, UseCaseResponse<Boolean>> {
}
