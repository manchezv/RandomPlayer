package com.manchezv.randomplayer.cases.numbers.calculation;

import com.manchezv.randomplayer.cases.UseCase;
import com.manchezv.randomplayer.cases.UseCaseResponse;
import com.manchezv.randomplayer.local.db.model.GeneratedNumberEntity;

import java.util.List;

public interface GetCalculationData extends UseCase<List<GeneratedNumberEntity>, UseCaseResponse<CalculationData>> {
}
