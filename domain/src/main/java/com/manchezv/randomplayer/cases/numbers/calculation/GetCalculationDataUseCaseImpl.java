package com.manchezv.randomplayer.cases.numbers.calculation;

import com.manchezv.randomplayer.cases.BaseUseCase;
import com.manchezv.randomplayer.cases.UseCaseResponse;
import com.manchezv.randomplayer.local.db.model.GeneratedNumberEntity;
import com.manchezv.randomplayer.notifications.NotCalculableNotification;
import com.manchezv.randomplayer.threading.MainThread;
import com.manchezv.randomplayer.threading.UseCaseExecutor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import javax.inject.Inject;

public class GetCalculationDataUseCaseImpl extends BaseUseCase<List<GeneratedNumberEntity>, UseCaseResponse<CalculationData>> implements GetCalculationData {

    @Inject
    public GetCalculationDataUseCaseImpl(UseCaseExecutor executor, MainThread mainThread) {
        super(executor, mainThread);
    }

    @Override
    protected void run(List<GeneratedNumberEntity> generatedNumberEntities) {
        if(generatedNumberEntities == null ||generatedNumberEntities.isEmpty()) {
            onResponse(new UseCaseResponse<>(NotCalculableNotification.getInstance()));
        } else {
            BigDecimal median = getMedian(generatedNumberEntities);
            BigDecimal desviation = getDesviation(median, generatedNumberEntities);

            onResponse(new UseCaseResponse<>(new CalculationData(median, desviation)));
        }
    }

    private BigDecimal getMedian(List<GeneratedNumberEntity> generatedNumberEntities) {
        int value = 0;
        for(GeneratedNumberEntity number : generatedNumberEntities) {
            value += number.getValue();
        }

        return BigDecimal.valueOf((double)value / generatedNumberEntities.size()).setScale(3, RoundingMode.HALF_EVEN);
    }

    private BigDecimal getDesviation(BigDecimal median, List<GeneratedNumberEntity> generatedNumberEntities) {
        double value = 0d;

        for (GeneratedNumberEntity number : generatedNumberEntities) {
            value += Math.pow((number.getValue() - median.doubleValue()), 2);
        }


            return BigDecimal.valueOf(Math.sqrt(value / generatedNumberEntities.size() - 1)).setScale(3, RoundingMode.HALF_EVEN);

    }
}
