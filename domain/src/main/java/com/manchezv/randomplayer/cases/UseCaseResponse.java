package com.manchezv.randomplayer.cases;

import com.manchezv.randomplayer.notifications.Notification;

public class UseCaseResponse<T> {

    private Notification notification;

    private T result;

    public UseCaseResponse(Notification notification) {
        this.notification = notification;
    }

    public UseCaseResponse(T result) {
        this.result = result;
    }

    public T getResult() {
        return result;
    }

    public boolean hasNotification() {
        return notification != null;
    }

    public Notification getNotification() {
        return notification;
    }
}

