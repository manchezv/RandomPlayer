package com.manchezv.randomplayer.cases;

import com.manchezv.randomplayer.threading.MainThread;
import com.manchezv.randomplayer.threading.UseCaseExecutor;

public abstract class BaseUseCase<Params, Response extends UseCaseResponse> implements UseCase<Params, Response> {

    private MainThread mainThread;
    private UseCaseExecutor executor;
    private UseCaseCallback<Response> useCaseCallback;

    public BaseUseCase(UseCaseExecutor executor, MainThread mainThread) {
        this.executor = executor;
        this.mainThread = mainThread;
    }

    public void execute(Params params, UseCaseCallback<Response> caseCallback){
        this.useCaseCallback = caseCallback;
        executor.execute(() -> BaseUseCase.this.run(params));
    }

    protected abstract void run(Params params);

    protected void onResponse(final Response useCaseResponse) {

        mainThread.runOnUiThread(() -> {

            if(useCaseCallback != null){
                useCaseCallback.onUseCaseResponse(useCaseResponse);
            }

        });

    }

}

