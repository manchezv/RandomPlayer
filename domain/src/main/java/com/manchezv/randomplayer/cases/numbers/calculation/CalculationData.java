package com.manchezv.randomplayer.cases.numbers.calculation;

import java.math.BigDecimal;

public class CalculationData {

    private BigDecimal median;

    private BigDecimal desviation;

    public CalculationData(BigDecimal median, BigDecimal desviation) {
        this.median = median;
        this.desviation = desviation;
    }

    public BigDecimal getMedian() {
        return median;
    }

    public BigDecimal getDesviation() {
        return desviation;
    }
}
