package com.manchezv.randomplayer.cases;

public interface UseCase<T, R extends UseCaseResponse> {

    void execute(T params, UseCaseCallback<R> caseCallback);

}

