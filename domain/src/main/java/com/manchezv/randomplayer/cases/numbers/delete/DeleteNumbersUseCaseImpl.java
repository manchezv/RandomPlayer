package com.manchezv.randomplayer.cases.numbers.delete;

import com.manchezv.randomplayer.cases.BaseUseCase;
import com.manchezv.randomplayer.cases.UseCaseResponse;
import com.manchezv.randomplayer.repositories.GeneratedNumbersRepository;
import com.manchezv.randomplayer.threading.MainThread;
import com.manchezv.randomplayer.threading.UseCaseExecutor;

import javax.inject.Inject;

public class DeleteNumbersUseCaseImpl extends BaseUseCase<Void, UseCaseResponse<Boolean>> implements DeleteNumbersUseCase {

    private final GeneratedNumbersRepository repo;

    @Inject
    public DeleteNumbersUseCaseImpl(UseCaseExecutor executor, MainThread mainThread, GeneratedNumbersRepository repo) {
        super(executor, mainThread);
        this.repo = repo;
    }

    @Override
    protected void run(Void params) {
        repo.deleteAll();
        onResponse(new UseCaseResponse<>(true));
    }
}
