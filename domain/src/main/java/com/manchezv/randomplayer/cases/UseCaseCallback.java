package com.manchezv.randomplayer.cases;

public interface UseCaseCallback<T extends UseCaseResponse> {

    void onUseCaseResponse(T response);
}

