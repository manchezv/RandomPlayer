package com.manchezv.randomplayer.local.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.manchezv.randomplayer.local.db.model.GeneratedNumberEntity;

import java.util.List;

@Dao
public interface GeneratedNumberDAO {

    @Insert
    void insert(GeneratedNumberEntity number);

    @Query("SELECT * from generated_number ORDER BY generated_timestamp DESC")
    LiveData<List<GeneratedNumberEntity>> getGeneratedNumbers();

    @Query("DELETE FROM generated_number")
    void deleteAll();
}
