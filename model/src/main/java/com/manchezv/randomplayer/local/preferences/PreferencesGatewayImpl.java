package com.manchezv.randomplayer.local.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.manchezv.randomplayer.R;

import javax.inject.Inject;

public class PreferencesGatewayImpl implements PreferencesGateway {

    private final SharedPreferences preferences;

    private static final String KEY_INTERVAL = "pref_interval_milisec";

    @Inject
    public PreferencesGatewayImpl(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    @Override
    public int getIntervalGenerationTime() {
        return Integer.valueOf(preferences.getString(KEY_INTERVAL, "500"));
    }
}
