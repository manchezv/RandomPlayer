package com.manchezv.randomplayer.local.db.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "generated_number")
public class GeneratedNumberEntity {

    @PrimaryKey(autoGenerate = true)
    private long id;

    private int value;

    @ColumnInfo(name = "generated_timestamp")
    private Date generatedTime;

    public GeneratedNumberEntity(int value, Date generatedTime) {
        this.value = value;
        this.generatedTime = generatedTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Date getGeneratedTime() {
        return generatedTime;
    }

    public void setGeneratedTime(Date generatedTime) {
        this.generatedTime = generatedTime;
    }
}
