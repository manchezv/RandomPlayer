package com.manchezv.randomplayer.local.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.manchezv.randomplayer.local.db.converter.DataTypesConverter;
import com.manchezv.randomplayer.local.db.model.GeneratedNumberEntity;

@Database(entities = {GeneratedNumberEntity.class}, version = 1)
@TypeConverters(value = {DataTypesConverter.class})
public abstract class RandomPlayerDatabase extends RoomDatabase {
    public abstract GeneratedNumberDAO getGeneratedNumberDao();
}
