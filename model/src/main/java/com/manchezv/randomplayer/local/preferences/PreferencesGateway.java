package com.manchezv.randomplayer.local.preferences;

public interface PreferencesGateway {
    int getIntervalGenerationTime();
}
