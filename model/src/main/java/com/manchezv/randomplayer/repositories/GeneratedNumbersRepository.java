package com.manchezv.randomplayer.repositories;

import android.arch.lifecycle.LiveData;

import com.manchezv.randomplayer.local.db.model.GeneratedNumberEntity;

import java.util.List;

public interface GeneratedNumbersRepository {
    void insert(GeneratedNumberEntity numberEntity);

    LiveData<List<GeneratedNumberEntity>> getNumberList();

    void deleteAll();
}
