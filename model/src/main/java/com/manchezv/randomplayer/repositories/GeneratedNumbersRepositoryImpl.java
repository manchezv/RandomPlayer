package com.manchezv.randomplayer.repositories;

import android.arch.lifecycle.LiveData;
import android.support.annotation.MainThread;
import android.support.annotation.WorkerThread;

import com.manchezv.randomplayer.local.db.GeneratedNumberDAO;
import com.manchezv.randomplayer.local.db.model.GeneratedNumberEntity;

import java.util.List;

import javax.inject.Inject;

public class GeneratedNumbersRepositoryImpl implements GeneratedNumbersRepository {

    private final GeneratedNumberDAO dao;

    @Inject
    public GeneratedNumbersRepositoryImpl(GeneratedNumberDAO dao) {
        this.dao = dao;
    }

    @Override
    @WorkerThread
    public void insert(GeneratedNumberEntity numberEntity) {
        dao.insert(numberEntity);
    }

    @Override
    @MainThread
    public LiveData<List<GeneratedNumberEntity>> getNumberList() {
        return dao.getGeneratedNumbers();
    }

    @Override
    public void deleteAll() {
        dao.deleteAll();
    }

}
